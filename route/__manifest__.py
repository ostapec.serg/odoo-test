# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.


{
    'name': 'Routes',
    'category': 'Sales/CRM',
    'sequence': 150,
    'summary': 'Centralize your orders',
    'description': """
This module gives you a quick view of your contacts directory, accessible from your home page.
You can track your vendors, customers and other contacts.
""",
    'depends': ['base', 'mail', 'hr', 'product'],
    'data': [
        'security/ir.model.access.csv',
        # 'views/route_order_line_views.xml',
        'views/route_order_views.xml',
        'views/route_car_views.xml',
        'views/route_product_views.xml',
        'views/route_routes_views.xml',
        'views/route_res_partner_views.xml',
        'views/route_menu_views.xml',
    ],
    'application': True,
    'license': 'LGPL-3',
    'assets': {
        'web.assets_tests': [
            'contacts/static/tests/tours/**/*',
        ],
    }
}
