# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from . import route_order, route_route, route_car, \
    route_product, route_order_line, route_res_partner
